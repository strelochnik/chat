﻿using Client.Classes;
using CommonLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chat
{
    public partial class Server : Form
    {
        Thread thread;
        Action<string, Color> AppendTextAction;
        Action<string, string> SuccessAction;
        int port;
        List<SMessage> allMessages;
        private ManualResetEvent resetEvent = new ManualResetEvent(false);

        public Server()
        {
            InitializeComponent();
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnExit);

            InputOutput.fileName = "server.json";
            allMessages = InputOutput.ReadFileData<SMessage>();

            IPAddresesTextBox.Text = NetworkHelper.GetIPAddress();

            if (allMessages.Count > 0) // Не обязательно
                foreach (SMessage message in allMessages)
                    MessagesGrid.Rows.Add(message.IpAddress, message.Date, message.Text);
        }

        /// <summary>
        /// Старт слушателя
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartButton_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(PortTextBox.Text, out port))
            {
                MessageBox.Show("Ошибка указания порта", "Ошибка конфигурации", MessageBoxButtons.OK);
                return;
            }

            //Вырубаем кнопки, от случайного нажатия
            ToggleButtons(false);

            AppendTextAction = new Action<string, Color>(UpdateResult);
            SuccessAction = new Action<string, string>(UpdateMessages);

            thread = new Thread(new ParameterizedThreadStart(ServerThread));
            thread.IsBackground = true;
            thread.Start();
        }

        /// <summary>
        /// Выключение сервера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopButton_Click(object sender, EventArgs e)
        {
            ToggleButtons();
            if (thread != null && thread.IsAlive)
            {
                Listener.listenSocket.Close();
                Listener.listenSocket.Dispose();
            }
        }

        /// <summary>
        /// Старт листнера в отдельном потоке
        /// </summary>
        /// <param name="StateInfo"></param>
        void ServerThread(Object StateInfo)
        {
            Listener.Start(AppendTextAction, SuccessAction, port);
        }

        /// <summary>
        /// От двойного нажатия
        /// </summary>
        /// <param name="enable"></param>
        void ToggleButtons(bool enable = true)
        {
            PortTextBox.Enabled =
                StartButton.Enabled =
                    PortTextBox.Enabled = enable;
            StopButton.Enabled = !enable;
        }

        /// <summary>
        /// Обновление текста
        /// </summary>
        /// <param name="text"></param>
        /// <param name="color"></param>
        void UpdateResult(string text, Color color)
        {
            Invoke((MethodInvoker)delegate
            {
                AppendText(StateTextBox, text, color);
            });
        }


        void UpdateMessages(string responseData, string ip)
        {
            Invoke((MethodInvoker)delegate
            {
                try
                {
                    //Плохо использовать dynamic, но в нашем случае это слабая связанность и независимость от клиента
                    var messages = JsonConvert.DeserializeObject<List<dynamic>>(responseData);
                    foreach (dynamic message in messages)
                    {
                        MessagesGrid.Rows.Add(ip, message.Date, message.Text);
                        allMessages.Add(new SMessage() { IpAddress = ip, Date = message.Date, Text = message.Text, Id = message.Id });
                    }
                }
                catch (Exception ex)
                {
                    AppendText(StateTextBox, ex.Message, Color.Red);
                }
            });
        }

        /// <summary>
        /// Хелпер для информационного окна
        /// </summary>
        /// <param name="box"></param>
        /// <param name="text"></param>
        /// <param name="color"></param>
        static void AppendText(RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;
            box.SelectionColor = color;
            box.AppendText(text + Environment.NewLine);
            box.ScrollToCaret();
        }

        /// <summary>
        /// При выходе сохраняем наши результаты в БД типа "файл"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnExit(object sender, EventArgs e)
        {
            InputOutput.WriteData(allMessages);
        }
    }
}
