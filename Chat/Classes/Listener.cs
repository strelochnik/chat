﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Drawing;
using CommonLibrary;

namespace Chat
{
    class Listener
    {
        public static Socket listenSocket;
        public static void Start(Action<string, Color> notyfyAction, Action<string, string> successAction, int port = 11000)
        {
            // получаем адреса для запуска сокета
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(NetworkHelper.GetIPAddress()), port);

            notyfyAction.Invoke("создаем сокет", Color.Black);

            // создаем сокет
            listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                // связываем сокет с локальной точкой, по которой будем принимать данные
                listenSocket.Bind(ipPoint);

                // начинаем прослушивание
                listenSocket.Listen(10);

                notyfyAction.Invoke("Сервер запущен. Ожидание подключений...", Color.Black);

                while (true)
                {
                    string recivedMessages;
                    string ip;
                    Socket handler = listenSocket.Accept();
                    // получаем сообщение
                    notyfyAction.Invoke("получаем сообщение", Color.Black);
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0; // количество полученных байтов
                    byte[] data = new byte[256]; // буфер для получаемых данных
                    do
                    {
                        bytes = handler.Receive(data);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (handler.Available > 0);

                    recivedMessages = builder.ToString();

                    notyfyAction.Invoke("отправляем ответ", Color.Black);
                    // Формируем ответ
                    string response = RequestStatus.Sent.ToString();
                    ip = ((IPEndPoint)handler.RemoteEndPoint).Address.ToString();

                    data = Encoding.Unicode.GetBytes(response);
                    handler.Send(data);
                    notyfyAction.Invoke("закрываем сокет", Color.Black);
                    // Закрываем сокет
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                    // Передаем основному потоку данные
                    successAction(recivedMessages, ip);
                }
            }
            catch (SocketException ex) when (ex.ErrorCode == 10004)
            {
                notyfyAction.Invoke("Операция прервана сокет закрыт", Color.Red);
            }
            catch (Exception ex)
            {
                notyfyAction.Invoke(ex.Message, Color.Red);
            }
        }
    }
}
