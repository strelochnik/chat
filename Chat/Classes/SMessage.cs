﻿using Client.Enums;

namespace Client.Classes
{
    class SMessage
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string IpAddress { get; set; }
        //public MessageStatus Status { get; set; }
        public string Text { get; set; }
    }
}
