﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Client.Classes
{
    class Sender
    {

        public static string Send(string message, IPAddress ipAddress, int port = 11000)
        {
            try
            {
                IPEndPoint ipPoint = new IPEndPoint(ipAddress, port);

                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                // Подключаемся к удаленному хосту
                socket.Connect(ipPoint);

                byte[] data = Encoding.Unicode.GetBytes(message);
                socket.Send(data);

                // Получаем ответ
                data = new byte[256]; // Буфер для ответа
                StringBuilder builder = new StringBuilder();
                int bytes = 0; // Количество полученных байт

                do
                {
                    bytes = socket.Receive(data, data.Length, 0);
                    builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                }
                while (socket.Available > 0);

                // Закрываем сокет
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
                // Возвращаем ответ
                return builder.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
