﻿using Client.Classes;
using Client.Enums;
using CommonLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace Client
{
    public partial class Client : Form
    {
        Thread thread;
        List<CMessage> allMessages;
        string messageText;
        IPAddress ipAddress;
        System.Threading.Timer timer;

        public Client()
        {
            InitializeComponent();
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnExit);
            
            // Предварительные установки IP адреса
            IPAddressTextBox.Text = NetworkHelper.GetIPAddress();
            IPAddress.TryParse(IPAddressTextBox.Text, out ipAddress);
            
            // Название "БД"
            InputOutput.fileName = "client.json";
            allMessages = InputOutput.ReadFileData<CMessage>();

            if (allMessages.Count > 0) // Не обязательно
                foreach (CMessage message in allMessages)
                    MessagesGrid.Rows.Add(message.Date, message.Status, message.Text);

            // Устанавливаем метод обратного вызова
            TimerCallback timerCallback = new TimerCallback(SendMessages);
            // Таймер, чтобы слать не отосланные сообщения (возможно стоит перенести на отдельную кнопку)
            timer = new System.Threading.Timer(timerCallback, allMessages, 0, 10000);
        }

        /// <summary>
        /// Кнопка отослать
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendButton_Click(object sender, EventArgs e)
        {
            if ((messageText = MessageTextBox.Text) == "")
                return;

            if (!IPAddress.TryParse(IPAddressTextBox.Text, out ipAddress))
            {
                MessageBox.Show($"Не " + (IPAddressTextBox.Text == "" ? "" : "верно") + " введен ip адрес сервера", "Ошибка конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageTextBox.Clear();

            CMessage message = new CMessage { Date = DateTime.Now.ToString(), Id = allMessages.Count, Text = messageText };
            allMessages.Add(message);
            MessagesGrid.Rows.Add(message.Date, message.Status, message.Text);

            StartThread(new List<CMessage>() { message }); //Небольшой костыль, как без них...
        }

        /// <summary>
        /// Фильтруем и шлем сообщения
        /// </summary>
        /// <param name="data"></param>
        void SendMessages(object data)
        {
            if (TaskInProgress())
                return;

            List<CMessage> messages = allMessages.Where(m => m.Status != MessageStatus.Sent).ToList();

            if (messages.Count > 0)
            {
                foreach (CMessage message in messages)
                    MessagesGrid.Rows[message.Id].Cells["Status"].Value = message.Status = MessageStatus.InProgress;

                StartThread(messages);
            }
        }

        /// <summary>
        /// Успешное выполнение таска
        /// </summary>
        /// <param name="messages"></param>
        void UpdateThread(object messages)
        {
            string response = Sender.Send(InputOutput.SerializeObject(messages as List<CMessage>), ipAddress);

            if (response == RequestStatus.Sent.ToString())
                UpdateSuccess(messages);
            else
                UpdateFailed(messages);
        }

        /// <summary>
        /// Успешно отослано
        /// </summary>
        /// <param name="data">messages</param>
        void UpdateSuccess(object data)
        {
            Invoke((MethodInvoker)delegate
            {
                List<CMessage> messages = data as List<CMessage>;
                foreach (CMessage message in messages)
                    MessagesGrid.Rows[message.Id].Cells["Status"].Value = message.Status = MessageStatus.Sent;

                //Избыточно, но если программа упадет, данные не будут потеряны
                InputOutput.WriteData(allMessages);
            });
        }

        /// <summary>
        /// Тут можно сделать обработку ошибок
        /// </summary>
        /// <param name="data">messages</param>
        void UpdateFailed(object data)
        {
            Invoke((MethodInvoker)delegate
            {
                List<CMessage> messages = data as List<CMessage>;
                foreach (CMessage message in messages)
                    MessagesGrid.Rows[message.Id].Cells["Status"].Value = message.Status = MessageStatus.SendFailed;
            });
        }

        /// <summary>
        /// Проверяем, если не работает, запускаем наш асинхронный вызов
        /// ThreadPool нам не нужен
        /// </summary>
        /// <param name="messages"></param>
        void StartThread(object messages)
        {
            if (TaskInProgress())
                return;

            thread = new Thread(new ParameterizedThreadStart(UpdateThread));
            thread.Start(messages);
        }

        /// <summary>
        /// Проверяем, что таск незапущен
        /// </summary>
        /// <returns></returns>
        bool TaskInProgress()
        {
            return thread != null && thread.ThreadState != ThreadState.Stopped;
        }

        /// <summary>
        /// При выходе сохраняем наши результаты в БД типа "файл"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnExit(object sender, EventArgs e)
        {
            InputOutput.WriteData(allMessages);
        }
    }
}
