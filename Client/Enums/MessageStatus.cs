﻿namespace Client.Enums
{
    enum MessageStatus
    {
        InQueue = 0,
        InProgress,
        Sent,
        SendFailed
    }
}
