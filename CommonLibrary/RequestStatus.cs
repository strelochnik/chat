﻿namespace CommonLibrary
{
    public enum RequestStatus
    {
        Sent,
        Error
    }
}
