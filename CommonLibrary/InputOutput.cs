﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Client.Classes
{
    public class InputOutput
    {
        public static string fileName;
        public static List<T> ReadFileData<T>(string file = null)
        {
            try
            {
                string text = File.ReadAllText(fileName, Encoding.UTF8);
                return text != "" ? JsonConvert.DeserializeObject<List<T>>(text) : new List<T>();
            }
            catch (Exception ex)
            {
                return new List<T>();
            }
        }

        public static void WriteData<T>(IList<T> data)
        {
            try
            {
                File.WriteAllText(fileName, SerializeObject(data));
            }
            catch (Exception ex)
            {
            }
        }

        public static string SerializeObject(object data)
        {
            return JsonConvert.SerializeObject(data);
        }

        public static List<T> DeserializeObject<T>(string data)
        {
            return JsonConvert.DeserializeObject<List<T>>(data);
        }
    }
}
