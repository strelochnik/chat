﻿using System;
using System.Net;
using System.Net.Sockets;

namespace CommonLibrary
{
    public class NetworkHelper
    {
        /// <summary>
        /// Возвращает первый попавшийся ip v4
        /// </summary>
        /// <returns></returns>
        public static string GetIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("У вас нет сети с IPv4");
        }
    }
}
